#!/usr/bin/env python3
"""Script to update modalities.bson with the fw-modality-classification.yaml key value.

modalities.bson is defined in core-api and is used to define the modalities. It can be
found at https://gitlab.com/flywheel-io/product/backend/core-api/-/blob/master/data/init_db/modalities.bson
"""

from argparse import ArgumentParser

import bson
import yaml


def update_bson_with_yaml(bson_file, yaml_file):
    """Convert the modalities BSON defined in core-api to YAML."""
    with open(bson_file, "rb") as fp:
        bson_data = fp.read()
    bson_data_l = bson.decode_all(bson_data)

    with open(yaml_file, "r") as yaml_file:
        yaml_data = yaml.safe_load(yaml_file)

    for d in bson_data_l:
        if d.get("active", False) is True and d["_id"] in yaml_data:
            d["classification"] = yaml_data[d["_id"]]

    out_bson_file = bson_file.replace(".bson", "_updated.bson")
    with open(out_bson_file, "wb") as fp:
        for data in bson_data_l:
            fp.write(bson.encode(data))


if __name__ == "__main__":
    parser = ArgumentParser(description="Update modalities.bson from YAML")
    parser.add_argument("--bson", help="Path to the modalities.bson to be updated")
    parser.add_argument("--yaml", help="Path to YAML file to update from")

    args = parser.parse_args()

    update_bson_with_yaml(args.bson, args.yaml)
