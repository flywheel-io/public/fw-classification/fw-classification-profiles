<!-- markdownlint-disable MD024 -->
# Release Notes

## 0.4.9

### Enhancement

- Add DistortionMap to regex for intent override, improve regex in general

## 0.4.8

### Bug fix

- Add 'TOF' to list of Features for Siemens Perfusion TOF acquisitions

## 0.4.7

### Bug fix

- Removed 'CBF' from features as it's not part of the classification schema.

## 0.4.6

### Enhancement

- Files with "localizer" or "scout" in the series description are now classified with
Intent: "Localizer".
- Similarly, files with "fmap" or "Fieldmap" in the series description/acquisition
labels are now classified with Intent: "Fieldmap".

### Bug Fix

- Fix for TOF Siemens files being classified as Intent: "Structural" instead of
"Perfusion" (Only for files which have CSA headers).

## 0.4.5

### Bug Fix

- Fix AC -> Attenuation Corrected in PT profile

## 0.4.4

### Bug Fix

- Import fallback profile to classify MR files lacking 'Measurement'

## 0.4.3

### Bug Fix

- Modify CT rules. Lists are needed so pydantic doesn't fail in file-classifier gear.

## 0.4.2

### Enhancement

- Improve CT profiles and add ground truth headers.

## 0.4.1

### Enhancement

- GEAR-4483 Add CT profile, first pass

### Bug Fix

- Resolve GEAR-0000 "Fix classify mr intent"
- GEAR-5387 Fix download headers file

## 0.4.0

### Enhancement

- Overhaul MR profiles.
- Add CT schema to classification definitions.
- Add bson2yaml and yaml2bson utilities
- Add Unit-Testing Framework

## 0.3.2

### Enhancement

- Add profile for the classification of mammography data (MG.yaml)

## 0.2.0

### Bug Fix

- Change blocks meant to add features/measurements/intents to evaluate all rules instead
  of first, to make the block work as expected
