# type: ignore
import json
import logging
from pathlib import Path

import dotty_dict
import pandas as pd
from fw_classification.classify import Profile, run_classification

PROFILES_ROOT = Path(__file__).parents[1] / "classification_profiles"
# Load assertions.csv file
ASSERTIONS_DF = pd.read_csv(Path(__file__).parent / "ground_truth" / "assertions.csv")

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)


def remove_nulls(d):
    return {k: v for k, v in d.items() if v is not None}


def read_json_file(input_path):
    log.debug(input_path)
    data = {}
    with open(input_path, "r", encoding="utf-8") as fp:
        data["file"] = json.load(fp, object_hook=remove_nulls)
    return data


def matching_classification(
    classification: dotty_dict.Dotty, expected_classification: pd.Series
) -> bool:
    """Check that the classification is the expected one in the assertions file.

    Args:
        classification (dotty_dict.Dotty): classification result of run_classification
        expected_classification (pd.Series): expected classification (row from "assertions.csv")

    Returns:
        bool indicating the classification matches or not the expected classification
    """
    expected_classification_keys = expected_classification["classification_key"].split(
        ","
    )
    expected_classification_values = expected_classification[
        "classification_value"
    ].split(",")

    if len(expected_classification_keys) != len(expected_classification_values):
        log.warning(
            "Classification mismatch. Reason: len(%s) != len(%s)",
            expected_classification_keys,
            expected_classification_values,
        )
        return False
    if classification.get("file.classification", {}):
        for key, expected_value in zip(
            expected_classification_keys, expected_classification_values
        ):
            if key not in classification["file.classification"]:
                log.warning(
                    "Classification mismatch: Key: '%s' not found in actual classification: '%s'. Likely not set during classification.",
                    key,
                    classification["file.classification"],
                )
                return False
            actual_value = classification["file.classification"][key]
            # Test if expected value is not actual value.
            if expected_value not in actual_value:
                log.warning(
                    "Classification mismatch: Expected value: '%s' does not match actual value: '%s' for key: '%s'",
                    expected_value,
                    actual_value,
                    key,
                )
                return False

    # If we made it up to here, we have a match:
    return True


def unexpected_classification(
    classification: dotty_dict.Dotty, expected_classification: pd.Series
) -> bool:
    """Check that the classification is in the list of expected classifications.

    Args:
        classification (dotty_dict.Dotty): classification result of run_classification
        expected_classification (pd.Series): expected classification (row from "assertions.csv")

    Returns:
        bool indicating the classification exists in the list of expected classifications
    """
    expected_classification_keys = expected_classification["classification_key"].split(
        ","
    )
    expected_classification_values = expected_classification[
        "classification_value"
    ].split(",")

    if len(expected_classification_keys) != len(expected_classification_values):
        log.warning(
            "Classification mismatch. Reason: len(%s) != len(%s)",
            expected_classification_keys,
            expected_classification_values,
        )
        return False
    if classification.get("file.classification", {}):
        for key, actual_value in classification["file.classification"].items():
            if not isinstance(actual_value, list):
                actual_value = [actual_value]
            for value in actual_value:
                if value not in expected_classification_values:
                    log.warning(
                        "Classification Mismatch: Key/Value pair (key: %s: value: %s) not in expected values (%s)",
                        key,
                        value,
                        expected_classification_values,
                    )
                    return False
    # If we made it up to here, all classification values exist in expected classifications:
    return True


profile_path = PROFILES_ROOT / "main.yaml"

profile = Profile(profile_path)

# Assert Valid Profile
assert not profile.errors

# Merge 'Feature' into  rows (csv is in long/tall/narrow format)
column_map = {col: "first" for col in ASSERTIONS_DF.columns}
column_map["classification_key"] = ",".join
column_map["classification_value"] = ",".join
assertions = (
    ASSERTIONS_DF.groupby(["file"], as_index=False).agg(column_map).reset_index()
)

# Run classification and assert for each file
for _, row in assertions.iterrows():
    log.debug(row["file"])
    input_data = read_json_file(Path(__file__).parent / "ground_truth" / row["file"])
    result, out_data = run_classification(profile_path, input_data)

    # Assert at least one profile block ran
    assert result is True

    # Assert matching classification
    assert matching_classification(out_data, row)
    assert unexpected_classification(out_data, row)
